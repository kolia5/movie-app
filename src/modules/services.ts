import { MResponce, MovieDesc, IMovie } from './types/services-types';

interface IMovieService {
  fetchApi<T>(api: string): Promise<T>;
  getMovieByName(name: string): Promise<IMovie[]>;
  getMovieByCond(condition: string, pageNumber: number): Promise<IMovie[]>;
}

export default class MovieService implements IMovieService {
  private baseUrl: string;
  private apiKey: string;

  constructor(){
    this.baseUrl = 'https://api.themoviedb.org/3';
    this.apiKey = 'bcdcee17a3e4929b62e4ca6c8d65c583';
  }

  private stringifyQuery(str: string): string {
    return str.replace(/ /g, '+');
  }

  private transformMovie(movie: MovieDesc): IMovie{
    return {
      id: movie.id,
      imagePath: movie.poster_path,
      title: movie.title,
      releaseDate: movie.release_date,
      overview: movie.overview
    }
  }

  async fetchApi<T>(api: string): Promise<T> {
    const res = await fetch(`${this.baseUrl}${api}`);
    if(!res.ok){
      throw new Error(`Could not fetch ${this.baseUrl}${api}` + `, received ${res.status}`)
    }
    return await res.json();
  }

  async getMovieByName(name: string): Promise<IMovie[]> {
    const query: string = this.stringifyQuery(name);
    const res: MResponce = await this.fetchApi<MResponce>(`/search/movie?api_key=${this.apiKey}&query=${query}`);
    const movies = res.results;
    return movies.map(this.transformMovie)
  }

  async getMovieByCond(condition: string, pageNumber: number): Promise<IMovie[]>{
    const res: MResponce = await this.fetchApi<MResponce>(`/movie/${condition}?api_key=${this.apiKey}&language=en-US&page=${pageNumber}`);
    const movies = res.results;
    return movies.map(this.transformMovie)
  }

}
