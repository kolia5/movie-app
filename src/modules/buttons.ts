
import { renderFilmsContainer, extendFilmsContainer } from './films-container';
import MovieService from './services';

const movieService = new MovieService();

const buttons = ():void => {
  const input: HTMLInputElement | null = document.querySelector('#search');
  const searchBtn: HTMLElement | null = document.querySelector('#submit');
  const popularBtn: HTMLElement | null = document.querySelector('#popular');
  const upcomingBtn: HTMLElement | null = document.querySelector('#upcoming');
  const topRatedBtn: HTMLElement | null = document.querySelector('#top_rated');
  const loadMore: HTMLElement | null = document.querySelector('#load-more');
  let filterBy = 'popular';
  let pageNumber = 1;

  searchBtn?.addEventListener('click', handleForm);
  popularBtn?.addEventListener('click', () => handleBtn('popular'));
  upcomingBtn?.addEventListener('click', () => handleBtn('upcoming'));
  topRatedBtn?.addEventListener('click', () => handleBtn('top_rated'));
  loadMore?.addEventListener('click', handleLoadMore)

  async function handleForm (): Promise <void> {
    const titleFilm: string | undefined = input?.value;
    if(titleFilm && input){
      try {
        const res = await movieService.getMovieByName(titleFilm);
        renderFilmsContainer(res);
      } catch(e) {
          console.log(e);
      } finally {
        input.value = ''
      }
    }
  }

  async function handleBtn(condition: string): Promise <void>{
    filterBy = condition;
    try {
      const res = await movieService.getMovieByCond(filterBy, pageNumber);
      renderFilmsContainer(res);
    } catch(e) {
      console.log(e);
    }
  }

  async function handleLoadMore(): Promise<void>{
    pageNumber = pageNumber + 1
    try {
      const res = await movieService.getMovieByCond(filterBy, pageNumber);
      extendFilmsContainer(res);
    } catch(e) {
      console.log(e);
    }
  }


};

export { buttons };
