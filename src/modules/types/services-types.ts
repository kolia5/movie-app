export type MovieDesc = {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: Date;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export type MResponce = {
  page: number;
  results: MovieDesc[];
  total_pages: number;
  total_result: number;
}

export interface IMovie {
  id: number;
  imagePath: string;
  overview: string;
  title: string;
  releaseDate: Date;
}
