import { IMovie } from './types/services-types';

import { createElement, createSVGImage } from '../helpers/domHelper';

export const createCard = (film: IMovie): HTMLElement => {
  const { imagePath, overview, releaseDate } = film;
  const baseUrlImg = 'https://image.tmdb.org/t/p/w500';

  const wrapper: HTMLElement = createElement({
    tagName: 'div',
    className: 'col-lg-3 col-md-4 col-12 p-2'
  });

  const card: HTMLElement = createElement({
    tagName: 'div',
    className: 'card shadow-sm'
  });

  const img: HTMLElement = createElement({
    tagName: 'img',
    attributes: {
      src: `${baseUrlImg}${imagePath}`
    }
  })

  const svg = createSVGImage();

  const cardBody = createElement({
    tagName: 'div',
    className: 'card-body'
  })

  const cardText = createElement({
    tagName: 'p',
    className: 'card-text truncate'
  })
  cardText.textContent = overview;

  const dFlexElem = createElement({
    tagName: 'div',
    className: 'd-flex justify-content-between align-items-center'
  })

  const small = createElement({
    tagName: 'small',
    className: 'text-muted'
  })
  small.textContent = String(releaseDate);
  dFlexElem.append(small);

  cardBody.append(cardText, dFlexElem)

  card.append(img, svg, cardBody);

  wrapper.append(card);

  return wrapper
}
