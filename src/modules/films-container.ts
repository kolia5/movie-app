import { createCard } from './card';
import { IMovie } from './types/services-types';

const container: HTMLElement | null = document.getElementById('film-container');

const renderCards = (wrapper: HTMLElement, items: IMovie[]) => {
  if(wrapper) {
    if(items.length > 0){
      // add cards to container
      items.forEach(film => {
        const card: HTMLElement = createCard(film);
        wrapper.append(card)
      })
    } else {
      wrapper.textContent = 'There is not films with current search name'
    }
  }
}

const renderFilmsContainer = (items: IMovie[]):void => {
  if(container){
    // clear container
    container.innerHTML = '';
    // and add cards
    renderCards(container, items)
  }
}

const extendFilmsContainer = (items: IMovie[]):void => {
  container ?  renderCards(container, items) : container;
}

export {
  renderFilmsContainer,
  extendFilmsContainer
}
