type DOMAttributes = {
  id?: string;
  src?: string;
  xmlns?: string;
  stroke?: string;
  fill?: string;
  width?: string;
  height?: string;
  viewBox?: string;
  'fill-rule'?: string;
  d?: string;
}

type NewElement = {
  tagName: string;
  className?: string;
  attributes?: DOMAttributes;
}

export function createElement(elemData: NewElement): HTMLElement {
  const { tagName, className, attributes={} } = elemData;

  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean); // Include only not empty className values after the splitting
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key as keyof  DOMAttributes ]!));
  return element;
}

export function createSVGImage(): HTMLElement{
  const svg = createElement({
    tagName: 'svg',
    className: 'bi bi-heart-fill position-absolute p-2',
    attributes: {
      xmlns: 'http://www.w3.org/2000/svg',
      stroke: 'red',
      fill: 'red',
      width: '50',
      height: '50',
      viewBox: '0 -2 18 22'
    }
  });
  const path = createElement({
    tagName: 'path',
    attributes: {
      'fill-rule': 'evenodd',
      d: 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z'
    }
  })

  svg.append(path);
  return svg;
}
